package Model;

import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable{
	public Account(int id, double balance, String type) {
		super();
		this.id = id;
		this.balance = balance;
		this.type=type;
	}
	public Account() {
	}
	public int id;
	public double balance;
	public static String type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
		notifyObservers(this.id);
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double val) {
		this.balance = val;
		notifyObservers(this.balance);
	}
}
