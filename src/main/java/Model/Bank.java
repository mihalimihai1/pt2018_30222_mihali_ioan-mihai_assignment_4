package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import Logic.BankProc;

public class Bank implements Serializable, BankProc{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Bank(HashMap<Persoana, Set<Account>> data1) {
		super();
		this.dat = data1;
	}
	public ArrayList<Persoana> owners;
	public HashMap<Persoana, Set<Account>> dat;
	public int ok;
	public void addPerson(Persoana person) {
		// TODO Auto-generated method stub
		Account account = new Account();
		ArrayList<Persoana> persoane = new ArrayList<Persoana>();
		try {
			owners.add(person);
			dat.get(person).add(account);
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null,"Eroare la adaugare!","",JOptionPane.INFORMATION_MESSAGE);
		}
	}
	public Boolean format(){
		if(dat == null)
			return false;
		else
			return true;
	}

	public void editPerson(int id, Persoana person) {
		// TODO Auto-generated method stub
		assert format();
		for(Persoana p:dat.keySet()) {
			if(p.getId()==id) {
				if(p.getNume()!=person.getNume())
					p.setNume(person.getNume());
				if(p.getTelefon()!=person.getTelefon())
					p.setTelefon(person.getTelefon());
				if(p.getId()!=person.getId())
					p.setId(person.getId());
			}
		}
	}

	public void deletePerson(Persoana person) {
		Object p = null;
		// TODO Auto-generated method stub
		assert person != null;
		int size = dat.size();
		assert format();
		dat.remove(person);
		assert format();
		assert size-1 == dat.size();
	}

	public void addAccount(Persoana person, Account newAccount) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> map = new HashMap<String, Integer>();
        
        print(map);
        map.put("vishal", 10);
        map.put("sachin", 30);
        map.put("vaibhav", 20);
         
        System.out.println("Size of map is:- " + map.size());
     
        print(map);
        if (map.containsKey("vishal")) 
        {
            Integer a = map.get("vishal");
            System.out.println("value for key \"vishal\" is:- " + a);
        }
         
        map.clear();
        print(map);
    }
	public static void print(Map<String, Integer> map) {
        if (map.isEmpty()) 
            System.out.println("map is empty");
        else
            System.out.println(map);
    }

	public void removeAccount(Persoana pers, Account account) {
		// TODO Auto-generated method stub
		ArrayList<Account> list = (ArrayList<Account>) this.dat.get(pers);
		dat.put(pers, (Set<Account>) list);
		((Map<Persoana, Set<Account>>) list).put(pers,(Set<Account>) list);
	}

	public void editAccount(Account account, double val) {
		// TODO Auto-generated method stub
		SpendingAccount p = (SpendingAccount) account;
		if(account.type=="SavingAccount") {
			account.setBalance(val);
		}else {
			if(account.type == "SpendingAccount") {
				account.setBalance(val-p.comission);
			}
		}	
	}

	public void editSavingAccount(Account account, double val) {
		// TODO Auto-generated method stub
		assert format();
		SavingAccount op = (SavingAccount) account;
		for(Persoana p : dat.keySet())
			for(Account a : dat.get(p))
				if(a.getId()==account.getId())
				{
					if(account.type == "SavingAccount")
						account.setBalance(val);
				}
	}

	public void editSpendingAccount(Account account, double val) {
		// TODO Auto-generated method stub
		assert format();
		SpendingAccount op = (SpendingAccount) account;
		for(Persoana p : dat.keySet())
			for(Account a : dat.get(p))
				if(a.getId()==account.getId())
				{
					if(account.type == "SavingAccount")
						account.setBalance(val);
				}
	}
	public HashMap<Persoana,Set<Account>> getData()
	{
		return this.dat;
		
	}

}
