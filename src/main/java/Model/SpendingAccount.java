package Model;

public class SpendingAccount extends Account{
	public SpendingAccount(int id, double balance,String type) {
		super(id, balance,type);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	public double comission;
	public double getComission() {
		return comission;
	}
	public void setComission(double comission) {
		this.comission = comission;
		notifyObservers(comission);
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
