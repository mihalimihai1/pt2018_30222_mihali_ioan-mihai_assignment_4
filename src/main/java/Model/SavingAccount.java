package Model;

public class SavingAccount extends Account{

	public SavingAccount(int id, double balance, double interest) {
		super(id, balance, type);
		this.interest = interest;
	}
	public SavingAccount(int id, double balance) {
		super(id, balance,type);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	public double interest;
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
		notifyObservers(interest);
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
