package Model;

public class Persoana {
	public Persoana(int id, String nume, String telefon) {
		super();
		this.id = id;
		this.nume = nume;
		this.telefon = telefon;
	}
	public int id;
	public String nume;
	public String telefon;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	@Override
	public String toString() {
		return "Persoana [id=" + id + ", nume=" + nume + ", telefon=" + telefon + "]";
	}
	
}
