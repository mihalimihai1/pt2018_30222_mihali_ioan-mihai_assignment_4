package Logic;

import java.security.Permission;

import Model.Account;
import Model.Persoana;

public interface BankProc {
	void addPerson(Persoana person);
	void editPerson(int id, Persoana person);
	void deletePerson(Persoana person);
	 void addAccount(Persoana person, Account newAccount);
	 void removeAccount(Persoana pers, Account account);
	 void editAccount(Account account, double val);
	 void editSavingAccount(Account account, double val);
	 void editSpendingAccount(Account account, double val);
}
