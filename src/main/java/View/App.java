package View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
/**
 * Hello world!
 *
 */
public class App {
	
	private JLabel id, nume, telefon, id_account, balance_account, type_account, comission, interest, amount;
	private JButton add_person, edit_person, delete_person, add_account, edit_account, delete_account, add_money, withdraw_money, back; 
	private JTextField text_id, text_nume, text_telefon, text_id_account, text_balance_account, text_comission, text_interest, text_amount;
	private JPanel panou = new JPanel();
	private JPanel panou1 = new JPanel();
	public JComboBox box;
	JFrame frame = new JFrame("Pagina Clienti");
	JFrame frame1 = new JFrame("Accounts");
	
	private JTable table = new JTable();
	 DefaultTableModel model = (DefaultTableModel) table.getModel();
	 
	 private JTable table1 = new JTable();
	 DefaultTableModel model1 = (DefaultTableModel) table1.getModel();
	
	public App() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(1, 2));
		frame.setSize(700, 500);
		frame.setBounds(150,100,800,300);
		panou.setLayout(null);
		
		id = new JLabel("ID:");
		nume = new JLabel("Nume:");
		telefon = new JLabel("Telefon:");
		
		id.setBounds(30,30,100,20);
		panou.add(id);
		text_id = new JTextField("");
		text_id.setBounds(50,30,60,20);
		panou.add(text_id);
		
		nume.setBounds(10,60,100,20);
		panou.add(nume);
		text_nume = new JTextField("");
		text_nume.setBounds(50,60,110,20);
		panou.add(text_nume);
		
		telefon.setBounds(3,90,100,20);
		panou.add(telefon);
		text_telefon = new JTextField("");
		text_telefon.setBounds(50,90,110,20);
		panou.add(text_telefon);
		
		add_person = new JButton("Add person");
		add_person.setBounds(10,120,130,30);
		panou.add(add_person);
		
		edit_person = new JButton("Edit person");
		edit_person.setBounds(10,160,130,30);
		panou.add(edit_person);
		
		delete_person = new JButton("Delete person");
		delete_person.setBounds(10,200,130,30);
		panou.add(delete_person);
		
		JScrollPane scrollPane = new JScrollPane(table);
		frame.add(scrollPane,BorderLayout.WEST);
		model.addColumn("Id");
        model.addColumn("Nume");
        model.addColumn("Telefon");
		frame.add(panou);
		
		frame.setVisible(true);
		////
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setLayout(new GridLayout(1, 2));
		frame1.setSize(800, 500);
		frame1.setBounds(150,100,800,300);
		panou1.setLayout(null);
		
		id_account = new JLabel("ID:");
		id_account.setBounds(30,30,100,20);
		panou1.add(id_account);
		text_id_account = new JTextField("");
		text_id_account.setBounds(50,30,60,20);
		panou1.add(text_id_account);
		
		balance_account = new JLabel("Balance:");
		balance_account.setBounds(0,60,100,20);
		panou1.add(balance_account);
		text_balance_account = new JTextField("");
		text_balance_account.setBounds(50,60,110,20);
		panou1.add(text_balance_account);
		
		
		
		interest = new JLabel("Interest:");
		interest.setBounds(0,90,100,20);
		panou1.add(interest);
		text_interest = new JTextField("");
		text_interest.setBounds(50,90,110,20);
		panou1.add(text_interest);
		
		comission = new JLabel("Comission:");
		comission.setBounds(0,120,100,20);
		panou1.add(comission);
		text_comission = new JTextField("");
		text_comission.setBounds(63,120,110,20);
		panou1.add(text_comission);
		
		amount = new JLabel("Amount:");
		amount.setBounds(0,150,100,20);
		panou1.add(amount);
		text_amount = new JTextField("");
		text_amount.setBounds(50,150,110,20);
		panou1.add(text_amount);
		
		type_account = new JLabel("Type:");
		type_account.setBounds(10,5,100,20);
		panou1.add(type_account);
		box = new JComboBox();
		box.setModel(new DefaultComboBoxModel(new String[] {"SpendingAccount","SavingAccount"}));
		box.setBounds(45,5,130,20);
		panou1.add(box);
		
		back = new JButton("Back");
		back.setBounds(250, 220, 100, 30);
		panou1.add(back);
		
		add_account = new JButton("Add account");
		add_account.setBounds(250, 20, 120, 30);
		panou1.add(add_account);
		
		edit_account = new JButton("Edit Account");
		edit_account.setBounds(250, 60, 120, 30);
		panou1.add(edit_account);
		
		delete_account = new JButton("Delete Account");
		delete_account.setBounds(250, 100, 120, 30);
		panou1.add(delete_account);
		
		add_money = new JButton("Add money");
		add_money.setBounds(250, 140, 120, 30);
		panou1.add(add_money);
		
		withdraw_money = new JButton("Withdraw mon.");
		withdraw_money.setBounds(250, 180, 120, 30);
		panou1.add(withdraw_money);
		
		JScrollPane scrollPane1 = new JScrollPane(table1);
		frame1.add(scrollPane1,BorderLayout.WEST);
		
		model1.addColumn("Id");
        model1.addColumn("Balance");
        model1.addColumn("Type");
        model1.addColumn("Comission/Interest");
        frame1.add(panou1);
        frame1.setVisible(false);
		
		add_person.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Object[] date = new Object[4];
				if(!text_id.getText().trim().equals("") && !text_nume.getText().trim().equals("") && !text_telefon.getText().trim().equals(""))
					model.addRow(new Object[] {text_id.getText(),text_nume.getText(),text_telefon.getText()});
				else 
					JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
			}
		});
		edit_person.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!text_id.getText().trim().equals("") && !text_nume.getText().trim().equals("") && !text_telefon.getText().trim().equals("")) {	
				if(table.getSelectedRow()==-1) {
					if(table.getRowCount()==0) {
						JOptionPane.showMessageDialog(frame, "Tabelul este gol!");
					}
					else {
						JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
					}
				}else {
					model.setValueAt(text_id.getText(), table.getSelectedRow(), 0);
					model.setValueAt(text_nume.getText(), table.getSelectedRow(), 1);
					model.setValueAt(text_telefon.getText(), table.getSelectedRow(), 2);
				}
			  }else {
				  JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
			  }
				
			}
		});
		delete_person.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!text_id.getText().trim().equals("") && !text_nume.getText().trim().equals("") && !text_telefon.getText().trim().equals("")) {	
					if(table.getSelectedRow()==-1) {
						if(table.getRowCount()==0) {
							JOptionPane.showMessageDialog(frame, "Tabelul este gol!");
						}
						else {
							JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
						}
					}else {
						model.removeRow(table.getSelectedRow());
					}
				  }else {
					  JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
				  }
			}
		});
		add_account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Object[] date = new Object[4];
				if(box.getSelectedItem().equals("SpendingAccount")) {
					if(!text_id_account.getText().trim().equals("") && !text_balance_account.getText().trim().equals(""))
						model1.addRow(new Object[] {text_id_account.getText(),text_balance_account.getText(),box.getSelectedItem(),text_comission.getText()});
					else 
						JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
			    }
				else
				{
					if(!text_id_account.getText().trim().equals("") && !text_balance_account.getText().trim().equals(""))
						model1.addRow(new Object[] {text_id_account.getText(),text_balance_account.getText(),box.getSelectedItem(),text_interest.getText()});
					else 
						JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
				}
			}
		});
		edit_account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		 if(box.getSelectedItem().equals("SpendingAccount")) {
				if(!text_id_account.getText().trim().equals("") && !text_balance_account.getText().trim().equals("")) {	
				if(table.getSelectedRow()==-1) {
					if(table.getRowCount()==0) {
						model1.setValueAt(text_id_account.getText(), table1.getSelectedRow(), 0);
						model1.setValueAt(text_balance_account.getText(), table1.getSelectedRow(), 1);
						model1.setValueAt(box.getSelectedItem(), table1.getSelectedRow(), 2);
						model1.setValueAt(text_comission.getText(), table1.getSelectedRow(), 3);
					//	JOptionPane.showMessageDialog(frame, "Tabelul este gol!");
					}
					else {
						JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
					}
				}else {}
			  }else {
				  JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
			  }
			}
		 else 
			{
			 if(!text_id_account.getText().trim().equals("") && !text_balance_account.getText().trim().equals("")) {	
					if(table.getSelectedRow()==-1) {
						if(table.getRowCount()==0) {
							//JOptionPane.showMessageDialog(frame, "Tabelul este gol!");
							model1.setValueAt(text_id_account.getText(), table1.getSelectedRow(), 0);
							model1.setValueAt(text_balance_account.getText(), table1.getSelectedRow(), 1);
							model1.setValueAt(box.getSelectedItem(), table1.getSelectedRow(), 2);
							model1.setValueAt(text_interest.getText(), table1.getSelectedRow(), 3);
						}
						else {
							JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
						}
					}else {}
				  }else {
					  JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
				  }
			}
		  
			
		}
		});
		delete_account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!text_id_account.getText().trim().equals("") && !text_balance_account.getText().trim().equals("")) {	
					if(table.getSelectedRow()==-1) {
						if(table.getRowCount()==0) {
							//JOptionPane.showMessageDialog(frame, "Tabelul este gol!");
							model1.removeRow(table1.getSelectedRow());
						}
						else {
							JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
						}
					}else {}
				  }else {
					  JOptionPane.showMessageDialog(frame, "Nu se lasa gol!");
				  }
			}
		});
		
		
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame1.setVisible(false);
				frame.setVisible(true);
			}
		});
		Handlerclass handler = new Handlerclass();
		panou.addMouseListener(handler);
		
	}
	
	private class Handlerclass implements MouseListener{
		public void mouseDoubleClicked(MouseEvent arg0) {
			
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseClicked(MouseEvent e) {
			if(table.getSelectedRow()==-1) {
				JOptionPane.showMessageDialog(frame, "Selecteaza un client!");
			}else {
				frame1.setVisible(true);
				frame.setVisible(false);
			}
			
		}
		
	}
	
    public static void main( String[] args )
    {
    	App meniu = new App();
        System.out.println( "Hello World!" );
    }
}
